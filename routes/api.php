<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

#section crud
Route::get('section/{id?}', 'SectionController@index')->name('section.index');
Route::post('section', 'SectionController@store')->name('section.store');
Route::put('section/{id}', 'SectionController@update')->name('section.update');
Route::delete('section/{id}', 'SectionController@destroy')->name('section.delete');

#tasks crud
Route::get('section/{idSection}/tasks/{id?}', 'TaskController@index')->name('task.index');
Route::post('section/{idSection}/tasks', 'TaskController@store')->name('task.store');
Route::put('section/{idSection}/tasks/{id}', 'TaskController@update')->name('task.update');
Route::delete('section/{idSection}/tasks/{id}', 'TaskController@destroy')->name('task.delete');
Route::get('section/{idSection}/tasks/{id}/done', 'TaskController@done')->name('task.done');
Route::get('section/{idSection}/tasks/{id}/undone', 'TaskController@undone')->name('task.undone');
