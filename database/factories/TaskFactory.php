<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Task;
use Faker\Generator as Faker;

$factory->define(Task::class, function (Faker $faker) {
    return [
        'section_id' => function () {
            return \App\Section::all()->random();
        },
        'name' => $faker->word,
        'status'        => 0,
        'created_at'    => NOW()
    ];
});
