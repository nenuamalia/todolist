<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class TaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');

        for ($i = 1; $i <= 10; $i++) {
            DB::table('tasks')->insert([
                'section_id'    => $faker->numberBetween(1, 10),
                'name'          => $faker->word,
                'status'        => 0,
                'created_at'    => NOW()
            ]);
        }
    }
}
