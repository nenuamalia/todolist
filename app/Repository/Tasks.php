<?php

namespace App\Repository;

use App\Section;
use Carbon\Carbon;

class Tasks
{
    const CACHE_KEY = 'TASKS';

    public function all($idSection)
    {
        $cacheKey = $this->getCacheKey("all.tasks.{$idSection}");

        return cache()->remember($cacheKey, Carbon::now()->addMinutes(3), function () use ($idSection) {
            $section = Section::findOrFail($idSection);
            return $section->tasks;
        });
    }

    public function get($idSection, $id)
    {
        $cacheKey = $this->getCacheKey("get.tasks.{$idSection}.{$id}");

        return cache()->remember($cacheKey, Carbon::now()->addMinutes(3), function () use ($idSection, $id) {
            $section = Section::findOrFail($idSection);
            return $section->tasks()->whereId($id)->first();
        });
    }

    public function getCacheKey($key)
    {
        $key = strtoupper($key);
        return self::CACHE_KEY . ".$key";
    }
}
