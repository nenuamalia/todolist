<?php

namespace App\Repository;

use App\Section;
use Carbon\Carbon;

class Sections
{
    const CACHE_KEY = 'SECTIONS';

    public function all($orderBy)
    {
        $cacheKey = $this->getCacheKey("all.sections.{$orderBy}");

        return cache()->remember($cacheKey, Carbon::now()->addMinutes(3), function () use ($orderBy) {
            return Section::orderby($orderBy, 'DESC')->get();
        });
    }

    public function get($id)
    {
        $cacheKey = $this->getCacheKey("get.section.{$id}");

        return cache()->remember($cacheKey, Carbon::now()->addMinutes(3), function () use ($id) {
            return Section::findOrFail($id);
        });
    }

    public function getCacheKey($key)
    {
        $key = strtoupper($key);
        return self::CACHE_KEY . ".$key";
    }
}
