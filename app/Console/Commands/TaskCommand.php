<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Section;
use App\Task;

class TaskCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tasks:insert {section_id} {task_name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a task from a command in the command line';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $section_id = $this->argument('section_id');
        $task_name  = $this->argument('task_name');

        $section    = Section::findOrFail($section_id);
        $task       = new Task(['name' => $task_name, 'status' => 0]);
        $section->tasks()->save($task);

        $this->info("
                Successfully create new Task with this credential
                Section ID : {$section_id}
                Name : {$task_name}
            ");
    }
}
