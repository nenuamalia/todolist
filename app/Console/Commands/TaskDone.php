<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Section;
use App\Task;

class TaskDone extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tasks:done {section_id} {task_id} {status_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Change the status of a task from the command line';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $section_id = $this->argument('section_id');
        $task_id    = $this->argument('task_id');
        $status_id  = $this->argument('status_id');

        $section = Section::findOrFail($section_id);
        $section->tasks()->whereId($task_id)->update(['status' => $status_id]);

        if ($status_id == 1)
            $status = 'DONE';
        else
            $status = 'TO DO';

        $this->info("
                Successfully change the status of task
                Section ID : {$section_id}
                Task ID : {$task_id}
                Status : {$status}
            ");
    }
}
