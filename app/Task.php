<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Task extends Model
{
    protected $fillable = ['name', 'status'];

    public function getStatusAttribute($value)
    {
        if ($value == 1)
            return "Done";
        else
            return "To Do";
    }

    public function getCreatedAtAttribute($value)
    {
        if (!empty($value))
            return Carbon::createFromTimeStamp(strtotime($value))->diffForHumans();
        else
            return $value;
    }

    public function getUpdatedAtAttribute($value)
    {
        if (!empty($value))
            return Carbon::createFromTimeStamp(strtotime($value))->diffForHumans();
        else
            return $value;
    }
}
