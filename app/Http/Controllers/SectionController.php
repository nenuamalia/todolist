<?php

namespace App\Http\Controllers;

use App\Section;
use Facades\App\Repository\Sections;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id = null)
    {
        if (!$id)
            $data = Sections::all('id');
        else
            $data = Sections::get($id);

        return $data;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'  => 'required|string|min:2'
        ]);

        $response = [
            'status'        => 'failure',
            'status_code'   => 500,
            'message'       => 'Bad Request',
            'errors'        => $validator->errors(),
        ];

        if ($validator->fails()) {
            return response()->json($response, 500, ['Content-Type' => 'application/json']);
        }

        $section = new Section;
        $section->name = $request['name'];
        $section->save();

        return "Section created successfully.";
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Section  $section
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|min:2'
        ]);

        $response = [
            'status'        => 'failure',
            'status_code'   => 500,
            'message'       => 'Bad Request',
            'errors'        => $validator->errors(),
        ];

        if ($validator->fails()) {
            return response()->json($response, 500, ['Content-Type' => 'application/json']);
        }
        $data = Section::findOrFail($id);
        $data->update($request->all());

        return 'Section updated successfully.';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Section  $section
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Section::findOrFail($id);
        $data->tasks()->delete();
        $data->delete();

        return 'Section has been deleted';
    }
}
