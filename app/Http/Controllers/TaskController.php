<?php

namespace App\Http\Controllers;

use App\Section;
use App\Task;
use Facades\App\Repository\Tasks;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($idSection, $id = null)
    {
        if (!$id) {
            $data       = Tasks::all($idSection);
        } else {
            $data       = Tasks::get($idSection, $id);
        }

        return $data;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $idSection)
    {
        $section = Section::findOrFail($idSection);

        $validator = Validator::make($request->all(), [
            'name'  => 'required|string|min:2',
        ]);

        $response = [
            'status'        => 'failure',
            'status_code'   => 500,
            'message'       => 'Bad Request',
            'errors'        => $validator->errors(),
        ];

        if ($validator->fails()) {
            return response()->json($response, 500, ['Content-Type' => 'application/json']);
        }

        $task = new Task(['name' => $request['name'], 'status' => 0]);
        $section->tasks()->save($task);

        return "Task created successfully.";
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idSection, $id)
    {
        $section = Section::findOrFail($idSection);

        $validator = Validator::make($request->all(), [
            'name'  => 'required|string|min:2'
        ]);

        $response = [
            'status'        => 'failure',
            'status_code'   => 500,
            'message'       => 'Bad Request',
            'errors'        => $validator->errors(),
        ];

        if ($validator->fails()) {
            return response()->json($response, 500, ['Content-Type' => 'application/json']);
        }

        $section->tasks()->whereId($id)->update(['name' => $request['name']]);

        return 'Task updated successfully.';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy($idSection, $id)
    {
        $section = Section::find($idSection);
        $section->tasks()->whereId($id)->delete();

        return 'Task has been deleted';
    }

    public function done($idSection, $id)
    {
        $section = Section::findOrFail($idSection);
        $section->tasks()->whereId($id)->update(['status' => 1]);

        return 'Task changed to Done successfully.';
    }

    public function undone($idSection, $id)
    {
        $section = Section::findOrFail($idSection);
        $section->tasks()->whereId($id)->update(['status' => 0]);

        return 'Task changed to To Do successfully.';
    }
}
