<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Section extends Model
{
    protected $fillable = ['name'];

    #one to many
    public function tasks()
    {
        return $this->hasMany('App\Task')->orderBy('id', 'DESC');
    }

    public function getCreatedAtAttribute($value)
    {
        if (!empty($value))
            return Carbon::createFromTimeStamp(strtotime($value))->diffForHumans();
        else
            return $value;
    }

    public function getUpdatedAtAttribute($value)
    {
        if (!empty($value))
            return Carbon::createFromTimeStamp(strtotime($value))->diffForHumans();
        else
            return $value;
    }
}
